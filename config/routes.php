<?php
use app\core\route\Routes;

Routes::get('/login', 'AuthController@showLoginForm')->name('login.show');
Routes::post('/login', 'AuthController@login')->name('login.do');
Routes::post('/logout', 'AuthController@logout')->name('logout');

Routes::get('/', 'TaskController@index')->name('home');
Routes::get('/tasks', 'TaskController@index')->name('tasks.index');
Routes::get('/tasks/create', 'TaskController@create')->name('tasks.create');
Routes::post('/tasks', 'TaskController@store')->name('tasks.store');
Routes::get('/tasks/{task}', 'TaskController@show')->name('tasks.show');
Routes::get('/tasks/edit/{task}', 'TaskController@edit')->name('tasks.edit');
Routes::put('/tasks/update/{task}', 'TaskController@update')->name('tasks.update');
