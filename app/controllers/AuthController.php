<?php

namespace app\controllers;


use app\core\Auth;
use app\core\BaseController;
use app\core\Request;
use app\core\route\Url;
use app\core\Validator;

class AuthController extends BaseController
{
    public function showLoginForm()
    {
        if (Auth::isAdmin()) {
            $this->redirect(Url::route('home'));
        }

        $this->render('auth/login');
    }

    public function login()
    {
        $data = Request::getPostData();

        if (Validator::validate($data, [
                'name' => ['required', 'string'],
                'password' => ['required', 'string'],
        ])) {
            if (Auth::login($data['name'], $data['password'])) {
                return $this->redirect(Url::route('home'));
            }
        }

        $this->redirect(Url::route('login.show'))->withValidateErrors('login.show');
    }

    public function logout()
    {
        Auth::logout();
        return $this->redirect(Url::route('login.show'));
    }
}