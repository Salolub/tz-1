<?php

namespace app\controllers;

use app\core\BaseController;
use app\core\Request;
use app\core\route\Url;
use app\core\Validator;
use app\models\Task;

class TaskController extends BaseController
{
    public $adminActions = ['edit', 'update'];

    public function index(Request $request)
    {
        if (!$page = $request->getQueryVal('page')) {
            $page = 1;
        }
        $data = Task::search(
            $request->getQueryVal('orderColumn'),
            $request->getQueryVal('orderDirection'),
            $page
        );

        $this->render('tasks/index', [
            'tasks' => $data['data'],
            '_meta' => $data['pagination']['meta'],
            '_links' => $data['pagination']['links'],
        ]);
    }

    public function create()
    {
        $this->render('tasks/create');
    }

    public function store(Request $request)
    {
        if (Validator::validate(
            $request->getPostData(), [
                'username' => ['required', 'string'],
                'email' => ['required', 'email'],
                'text' => ['required', 'string'],
            ]
        )) {
            if (Task::create($request->getPostData())) {
                $_SESSION['flashMessage'] = 'Task created';
            }
            return $this->redirect(Url::route('tasks.index'));
        } else {
            return $this->redirect(Url::route('tasks.create'))->withValidateErrors('tasks.create');
        }
    }

    public function show(Task $task)
    {
        $this->render('tasks/show', [
            'task' => $task,
        ]);
    }

    public function edit(Task $task)
    {
        request()->setPostData($task->attributes);

        $this->render('tasks/edit', [
            'task' => $task,
        ]);
    }

    public function update(Request $request, Task $task)
    {
        if (!Validator::validate(
            $request->getPostData(), [
                'username' => ['required', 'string'],
                'email' => ['required', 'email'],
                'text' => ['required', 'string'],
            ]
        )) {
           return $this->redirect(Url::route('tasks.edit', ['task' => $task->id]))
                ->withValidateErrors('tasks.edit');
        }
        if ($task->text != $request->getPostVal('text')) {
            $task->edited = 1;
        }
        $task->fill($request->getPostData());
        $completed = false;
        if ($request->inPost('btnComplete')) {
            $task->status = 1;
            $completed = true;
        }
        $result = $task->update();
        if (!$completed) {
            $_SESSION['flashMessage'] = $result ? 'Task updated' : 'Task was not changed by user, nothing to update';
            return $this->redirect(Url::route('tasks.index'));
        } else {
            $_SESSION['flashMessage'] = 'Task completed';
            return $this->redirect(Url::route('tasks.edit', ['task' => $task->id]));
        }
    }
}