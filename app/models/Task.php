<?php

namespace app\models;


use app\core\db\BaseModel;

class Task extends BaseModel
{
    protected $tableName = 'task';
    protected $fillable = ['username', 'email', 'text'];

}