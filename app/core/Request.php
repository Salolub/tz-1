<?php

namespace app\core;


class Request
{
    private static $instance = null;
    private static $method;
    private static $route;
    private static $get = [];
    private static $post = [];
    private static $errors = [];

    public static function getInstance()
    {
        if (static::$instance == null) {
            self::$instance = new Request();
            if (!$_SESSION['csrf']) {
                self::generateCsrf();
            }
        }
        return self::$instance;
    }

    public static function handle()
    {
        $instance = self::getInstance();
        $instance::$method = $instance->getRequestMethod();
        if (!$instance->checkCsrf()) {
            throw new \Exception('Wrong CSRF token.');
        }
        $instance->parseQueryVars();
        if ($instance::$method != 'get') {
            $instance->fillPostData();
        }
    }

    public static function hasErrors()
    {
        return !empty(self::$errors);
    }

    public static function getErrors()
    {
        return self::$errors;
    }

    public static function clearErrors()
    {
        self::$errors = [];
    }

    public static function getMethod()
    {
        return self::$method;
    }

    public static function getQueryParams()
    {
        return self::$get;
    }

    public static function getPostData()
    {
        return self::$post;
    }

    public static function clearSystemPostData()
    {
        unset(self::$post['_csrf']);
        unset(self::$post['_method']);
    }

    public static function setPostData($data)
    {
        self::$post = array_merge(self::$post, $data);
    }

    public static function inPost($name)
    {
        return array_key_exists($name, self::$post);
    }

    public static function getQueryVal($name)
    {
        return !empty(self::$get[$name]) ? self::$get[$name] : '';
    }

    public static function getPostVal($name)
    {
        return !empty(self::$post[$name]) ? self::$post[$name] : '';
    }

    public static function getRoute()
    {
        return self::$route;
    }

    public static function setRoute($route)
    {
        self::$route = $route;
    }

    public static function setErrors($data)
    {
        self::$errors = $data;
    }

    public static function error($name, $val = '')
    {
        if (!empty($val)) {
            self::$errors[$name][] = $val;
            return $val;
        } else {
            return !empty(self::$errors[$name]) ? self::$errors[$name][0] : '';
        }
    }

    public static function generateCsrf()
    {
        $_SESSION['csrf'] = substr(md5(rand(2000, 10000)), 0, 16);
    }

    public static function csrf()
    {
        return $_SESSION['csrf'];
    }

    public function checkCsrf()
    {
        $oldCsrf = self::csrf();

        if (self::$method == 'get') {
            return true;
        } else {
            self::generateCsrf();
            return $oldCsrf == $_POST['_csrf'];
        }
    }

    private function getRequestMethod()
    {
        $allowedMethods = ['patch', 'put'];
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            return 'get';
        }
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (empty($_POST['_method'])) {
                return 'post';
            }
            if (in_array($_POST['_method'], $allowedMethods)) {
                return $_POST['_method'];
            }
        }
        throw new \Exception("Unsupported request method {$_POST['_method']}.");
    }

    private function parseQueryVars()
    {
        self::$get = [];
        if ($_SERVER['QUERY_STRING']) {
            $items = explode(';', $_SERVER['QUERY_STRING']);
            foreach ($items as $item) {
                $pair = explode('=', $item);
                self::$get[$pair[0]] = $pair[1];
            }
        }
    }

    private function fillPostData()
    {
        self::$post = [];
        if ($data = $_POST) {
            foreach ($data as $k => $v) {
                self::$post[$k] = trim(htmlspecialchars($v));
            }
        }
    }
}