<?php

namespace app\core;


use app\core\db\BaseModel;
use app\core\route\Router;
use app\core\route\Routes;

class App
{
    private static $instance = null;
    private static $pdo = null;

    /**
     * @var Config
     */
    private static $config;

    protected function __construct() {}
    protected function __clone() {}

    public static function debug($val)
    {
        echo "<pre>";
        print_r($val);
        echo "</pre>";
    }

    private static function getInstance()
    {
        if (static::$instance == null) {
            self::$instance = new App();
            session_start();
        }
        return self::$instance;
    }

    public static function init($config)
    {
        $instance = self::getInstance();
        self::$config = new Config($config);
        return $instance;
    }

    public static function config($key)
    {
        return self::$config->config($key);
    }

    /**
     * @return \PDO
     */
    public static function getPdo()
    {
        if (empty(self::$pdo)) {
            $data = self::config('db');
            self::$pdo = new \PDO($data['dsn'], $data['username'], $data['password']);
        } else {
        }

        return self::$pdo;
    }

    public static function closePdo()
    {
        self::$pdo = null;
    }

    public function run()
    {
        Request::handle();
        $route = $this->defineRoute();
        Request::setRoute($route);
        Request::clearSystemPostData();
        $this->doAction($route['action'], $route['param']);
    }

    private function defineRoute()
    {
        Routes::getInstance();
        $router = new Router();
        $route = $router->findRoute();
        if (!$route) {
            throw new \Exception('Page not found', 404);
        }
        return $route;
    }

    private function doAction($action, $routeParam = [])
    {
        $arr = explode('@', $action);
        $controllerClass = 'app\controllers\\' . $arr[0];
        $method = $arr[1];
        if (!method_exists($controllerClass, $method)) {
            throw new \Exception("Unknown method '$method' in $controllerClass");
        }
        $controller = new $controllerClass;
        $controller->checkUserAction($method);
        $controller->checkRedirectWithError();
        $params = $this->getMethodParams($controllerClass, $method, $routeParam);
        call_user_func_array([$controller, $method], $params);
    }

    private function getMethodParams($class, $method, $routeParam = [])
    {
        $item = new \ReflectionMethod($class, $method);
        $params = $item->getParameters();
        $result = [];
        foreach ($params as $param) {
            if (!$param->getClass()) {
                continue;
            }
            if ($param->getClass()->name == Request::class) {
                $result[] = Request::getInstance();
            } elseif ($param->getClass()->getParentClass()->name == BaseModel::class) {
                if ($routeParam['name'] != $param->name) {
                    throw new \Exception($param->name . ' variable is required tor method "' . $method
                        . '", but ' . $routeParam['name'] . ' received');
                }
                $modelClass = $param->getClass()->name;
                $result[] = $model = $modelClass::find($routeParam['value']);
            }
        }
        return $result;
    }
}
