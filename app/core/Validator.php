<?php

namespace app\core;


class Validator
{
    const RULE_REQUIRED = 'required';
    const RULE_STRING = 'string';
    const RULE_EMAIL = 'email';

    public static function validate($data, $rules)
    {
        Request::clearErrors();
        foreach ($data as $k => $v) {
            if (!empty($rules[$k])) {
                foreach ($rules[$k] as $rule) {
                    $result = self::checkRule($v, $rule);
                    if (true !== $result) {
                        Request::error($k, ucfirst($k) . ' ' . $result);
                        break;
                    }
                }
            }
        }

        return !Request::hasErrors();
    }

    private static function checkRule($value, $rule)
    {
        if (($rule == self::RULE_REQUIRED) && empty($value)) {
            return 'is required';
        }
        if (($rule == self::RULE_STRING)) {
            if (strlen($value) > 255) {
                return 'can not be longer than 255 symbols';
            }
        }
        if (($rule == self::RULE_EMAIL)) {
            if (1 !== preg_match('/\w+@\w+(\.\w+)+/i', $value)) {
                return 'must be a valid email';
            }
        }

        return true;
    }
}