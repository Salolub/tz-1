<?php

namespace app\core;


class Auth
{
    public static function login($name, $password)
    {
        if (($name == 'admin') && ($password == '123')) {
            $_SESSION['user'] = $name;
            return true;
        } else {
            Request::error('name', 'Wrong username or password');
            Request::error('password', 'Wrong username or password');
            return false;
        }
    }

    public static function logout()
    {
        $_SESSION['user'] = '';
    }

    public static function isAdmin()
    {
        return $_SESSION['user'] == 'admin';
    }

    public static function user()
    {
        return $_SESSION['user'] ? $_SESSION['user'] : '';
    }
}