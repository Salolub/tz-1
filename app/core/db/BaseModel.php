<?php

namespace app\core\db;


use app\core\App;
use app\core\Paginator;

class BaseModel
{
    protected $tableName;
    protected $fillable = [];
    protected $attributes = [];

    public function __construct($data = [])
    {
        foreach ($this->fillable as $field) {
            if (!empty($data[$field])) {
                $this->attributes[$field] = $data[$field];
            }
        }
    }

    public function __set($name, $value)
    {
        if (property_exists($this, $name)) {
            $this->$name = $value;
        } else {
            $this->attributes[$name] = $value;
        }
    }

    public function __get($name)
    {
        if (property_exists($this, $name)) {
            return $this->$name;
        } elseif (isset($this->attributes[$name])) {
            return $this->attributes[$name];
        } else {
            throw new \Exception("Unknown property '$name'");
        }
    }

    public static function search($orderColumn, $orderDirection, $page)
    {
        $tableName = static::getTableName();
        $query = 'SELECT COUNT(id) FROM ' . $tableName;
        $totalCount = pdo()->query($query)->fetchColumn(0);

        $query = 'SELECT * FROM ' . $tableName;

        if ($orderColumn) {
            $query .= ' ORDER BY ' . $orderColumn . ' ' . ($orderDirection == 'desc' ? 'desc' : 'asc');
        }
        if (!$perPage = App::config('perPage')) {
            $perPage = 5;
        }
        $query .= ' LIMIT ' . (($page - 1) * $perPage) . ',' . $perPage;
        $result = pdo()->query($query)->fetchAll(\PDO::FETCH_CLASS, static::class);
        App::closePdo();

        $paginator = (new Paginator())->setPage($page)
            ->setPerPage($perPage)
            ->setTotalCount($totalCount);

        return [
            'data' => $result,
            'pagination' => $paginator->getData(),
        ];

    }

    public static function create($data)
    {
        $model = new static($data);
        $queryData = $model->buildInsertData();
        $query = 'INSERT INTO ' . $model->tableName
            . ' (' . implode(',', $queryData['fields'])
            . ') VALUES (' . implode(',', $queryData['values']) . ');';

        $result = pdo()->exec($query);
        App::closePdo();

        return $result;
    }

    public static function find($id)
    {
        $tableName = static::getTableName();
        $query = 'SELECT * FROM ' . $tableName . ' WHERE id=' . $id;
        $result = pdo()->query($query)->fetchObject(static::class);
        App::closePdo();

        return $result;
    }

    public function update()
    {
        $query = 'UPDATE ' . $this->tableName
            . ' SET ' . $this->buildUpdateData()
            . ' WHERE id=' . $this->id;
        $result = pdo()->exec($query);
        App::closePdo();

        return $result;
    }

    public function fill($data)
    {
        foreach ($this->attributes as $k => $v) {
            if (isset($data[$k])) {
                $this->attributes[$k] = $data[$k];
            }
        }
    }

    protected static function getTableName()
    {
        return (new static)->tableName;
    }

    protected function buildInsertData()
    {
        $result = [
            'fields' => [],
            'values' => [],
        ];
        foreach ($this->attributes as $k => $v) {
            $result['fields'][] = $k;
            $result['values'][] = pdo()->quote($v);
        }

        return $result;
    }

    protected function buildUpdateData()
    {
        $result = [];
        foreach ($this->attributes as $k => $v) {
            $result[] = $k . '=' . pdo()->quote($v);
        }

        return implode(',', $result);
    }
}