<?php

namespace app\core\route;


use app\core\App;
use app\core\Config;
use app\core\Request;

class Router
{
    public function findRoute()
    {
        $uri = $this->getRealUri();
        return $this->searchRoute($uri);
    }

    private function getRealUri()
    {
        $homeUrl = App::config(Config::HOME_URL);
        $uri = $_SERVER['REQUEST_URI'];
        $pos = strpos($uri, '?');
        if (false !== $pos) {
            $uri = substr($uri, 0, $pos);
        }
        if (strlen($homeUrl) < 2) {
            return $uri;
        }
        $host = $_SERVER['HTTP_HOST'];
        $homeUrl = str_replace($host, '', $homeUrl);
        $realUri = str_replace($homeUrl, '', $uri);
        $realUri = str_replace('//', '/', $realUri);
        if ($realUri[0] != '/') {
            $realUri = '/' . $realUri;
        }
        return $realUri;
    }

    private function searchRoute($uri)
    {
        $method = Request::getMethod();
        if ($routes = Routes::getRoutesByMethod($method)) {
            foreach ($routes as $route) {
                $pos = strpos($route['uri'], '{');
                if ($pos >= 1) {
                    if ($this->compareWithParam($uri, $route)) {
                        return $route;
                    } else {
                        continue;
                    }
                } else {
                    if ($uri == $route['uri']) {
                        return $route;
                    }
                }
            }
        }

        return false;
    }

    private function compareWithParam($uri, &$route)
    {
        $uriArr = explode('/', $uri);
        $routeUriArr = explode('/', $route['uri']);
        $cnt = count($uriArr);
        $route['param'] = [];
        if ($cnt != count($routeUriArr)) {
            return false;
        }
        for ($i = 0; $i < $cnt - 1; $i++) {
            if ($uriArr[$i] != $routeUriArr[$i]) {
                return false;
            }
        }
        $route['param'] = [
            'name' => str_replace(['{', '}'], '', $routeUriArr[$cnt - 1]),
            'value' => $uriArr[$cnt - 1],
        ];

        return true;
    }

}