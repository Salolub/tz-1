<?php

namespace app\core\route;


use app\core\App;
use app\core\Config;
use app\core\Request;

class Url
{
    public static function route($name, $params = [])
    {
        $route = Routes::getRouteByName($name);
        $pos = strpos($route['uri'], '{');
        if ($pos !== false) {
            $url = substr($route['uri'], 0, $pos);
            $paramName = substr($route['uri'], $pos + 1, -1);
            if (empty($params[$paramName])) {
                throw new \Exception("Param '$paramName' is required fo route '$name'");
            }
            $url .= $params[$paramName];
            unset($params[$paramName]);
        } else {
            $url = $route['uri'];
        }
        self::applyHomeUrl($url);
        if (!$params) {
            return $url;
        }
        $queryParams = '';
        foreach ($params as $k => $v) {
            $queryParams .= $k . '=' . $v . ';';
        }

        return $url . '?' . substr($queryParams, 0, -1);
    }

    public static function getOrderParamsForField($field)
    {
        $q = Request::getQueryParams();
        $params = [];
        if (!empty($q['page'])) {
            $params['page'] = $q['page'];
        }
        $params['orderColumn'] = $field;
        $params['orderDirection'] = self::getOrderDirectionForField($field);

        return $params;
    }

    public static function getOrderDirectionForField($field)
    {
        return (Request::getQueryVal('orderColumn') == $field)
            && (Request::getQueryVal('orderDirection') == 'asc')
                ? 'desc' : 'asc';
    }

    private static function applyHomeUrl(&$url)
    {
        $homeUrl = App::config(Config::HOME_URL);
        if ($homeUrl == '/') {
            return;
        }
        $host = $_SERVER['HTTP_HOST'];
        $homeUrl = '/' . str_replace($host, '', $homeUrl);
        $url = str_replace('//', '/', $homeUrl . $url);

    }
}