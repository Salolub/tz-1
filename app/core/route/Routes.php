<?php

namespace app\core\route;

class Routes
{
    private static $instance = null;
    private static $routes = [];
    private static $key = 'get';

    public static function getInstance()
    {
        if (static::$instance == null) {
            self::$instance = new Routes();
            require dirname(__DIR__) . '/../../config/routes.php';
        }
        return self::$instance;
    }

    public static function get($uri, $action)
    {
        self::$key = 'get';
        self::addRow($uri, $action);
        return self::$instance;
    }

    public static function post($uri, $action)
    {
        self::$key = 'post';
        self::addRow($uri, $action);
        return self::$instance;
    }

    public static function put($uri, $action)
    {
        self::$key = 'put';
        self::addRow($uri, $action);
        return self::$instance;
    }

    public function name($name)
    {
        self::$routes[self::$key][count(self::$routes[self::$key]) - 1]['name'] = $name;
    }

    public static function getRoutesByMethod($method)
    {
        return self::$routes[$method];
    }

    public static function getRouteByName($name)
    {
        foreach (self::$routes as $k => $items) {
            foreach ($items as $item) {
                if ($item['name'] == $name) {
                    return $item;
                }
            }
        }
        throw new \Exception("Unknown route name '$name'");
    }

    private static function addRow($uri, $action)
    {
        self::$routes[self::$key][] = [
            'uri' => $uri,
            'action' => $action,
        ];
    }
}
