<?php

namespace app\core;


class Config
{
    const HOME_URL = 'homeUrl';
    const DB = 'db';

    private $config = [];

    public function __construct($config)
    {
        $this->config[self::HOME_URL] = isset($config[self::HOME_URL])
            ? $config[self::HOME_URL]
            : '/';

        unset($config[self::HOME_URL]);

        if (!isset($config[self::DB])) {
            die('There is no \'db\' key in config data.');
        } else {
            $this->setDbConfig($config[self::DB]);
        }
        unset($config[self::DB]);

        foreach ($config as $k => $v) {
            $this->config[$k] = $v;
        }

        return $this;
    }

    private function setDbConfig($dbConfig)
    {
        if (!isset($dbConfig['dsn'])) {
            die('There is no \'dsn\' key in database config data.');
        }
        if (!isset($dbConfig['username'])) {
            die('There is no \'username\' key in database config data.');
        }
        if (!isset($dbConfig['password'])) {
            die('There is no \'password\' key in database config data.');
        }
        $this->config[self::DB] = [
            'dsn' => $dbConfig['dsn'],
            'username' => $dbConfig['username'],
            'password' => $dbConfig['password'],
        ];
    }

    public function config($key)
    {
        return isset($this->config[$key]) ? $this->config[$key] : '';
    }
}