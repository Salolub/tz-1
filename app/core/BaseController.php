<?php

namespace app\core;


class BaseController
{
    public $adminActions = [];

    public function checkUserAction($action) {
        if (in_array($action, $this->adminActions) && !Auth::isAdmin()) {
            throw new \Exception('Only admin can modify a task');
        }
    }

    public function checkRedirectWithError()
    {
        if (isset($_SESSION['redirectData'])) {
            $data = json_decode($_SESSION['redirectData'], true);
            if ($data['route'] == Request::getRoute()['name']) {
                Request::setErrors($data['errors']);
                Request::setPostData($data['userInput']);
            }
            unset($_SESSION['redirectData']);
        }
    }

    public function render($view, $data = [])
    {
        if (isset($_SESSION['flashMessage'])) {
            $flashMessage = $_SESSION['flashMessage'];
            unset($_SESSION['flashMessage']);
        }
        $subView = dirname(__DIR__) . '/../views/' . $view . '.php';
        foreach ($data as $k => $v) {
            $$k = $v;
        }
        require dirname(__DIR__) . '/../views/layuots/main.php';
    }

    public function redirect($url, $statusCode = 303)
    {
        header('Location: ' . $url, true, $statusCode);
        return $this;
    }

    public function withValidateErrors($route)
    {
        $data = Request::getPostData();
        $_SESSION['redirectData'] = json_encode([
            'route' => $route,
            'userInput' => $data,
            'errors' => Request::getErrors(),
        ]);

        return $this;
    }
}