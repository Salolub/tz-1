<?php

namespace app\core;


use app\core\route\Url;

class Paginator
{
    private $totalCount;
    private $page;
    private $perPage;

    public function __construct()
    {
        return $this;
    }

    public function setPage($value)
    {
        $this->page = $value > 0 ? $value : 1;
        return $this;
    }

    public function setTotalCount($value)
    {
        $this->totalCount = (int)$value;
        return $this;
    }

    public function setPerPage($value)
    {
        $this->perPage = $value > 0 ? $value : 1;
        return $this;
    }

    public function getData()
    {
        $totalPages = ceil($this->totalCount / $this->perPage);
        $route = Request::getRoute();
        $q = Request::getQueryParams();
        $links = [];

        $q['page'] = 1;
        $links['first'] = Url::route($route['name'], $q);

        $q['page'] = $totalPages;
        $links['last'] = Url::route($route['name'], $q);

        $q['page'] = $this->page > 1 ? $this->page - 1 : 1;
        $links['prev'] = Url::route($route['name'], $q);

        $q['page'] = $this->page < $totalPages ? $this->page + 1 : $totalPages;
        $links['next'] = Url::route($route['name'], $q);

        return [
            'meta' => [
                'page' => $this->page,
                'perPage' => $this->perPage,
                'totalPages' => $totalPages,
                'totalCount' => $this->totalCount,
            ],
            'links' => $links,
        ];
    }
}