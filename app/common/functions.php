<?php
/**
 * @return PDO
 */
function pdo() {
    return \app\core\App::getPdo();
};

/**
 * @return \app\core\Request
 */
function request() {
    return \app\core\Request::getInstance();
};