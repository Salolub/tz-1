<h1>Test Framework</h1>
<div class="col-sm-offset-4 col-sm-4">
<form action="<?= \app\core\route\Url::route('login.do') ?>" method="POST">
    <input type="hidden" name="_csrf" value="<?= \app\core\Request::csrf() ?>">
    <div>
        <label for="name">Name</label>
        <input type="text" id="name" name="name" class="form-control"
               value="<?= \app\core\Request::getPostVal('name')?>">
        <div class="text-danger"><?= \app\core\Request::error('name') ?></div>
    </div>
    <div>
        <label for="password">Password</label>
        <input type="password" id="password" name="password" class="form-control">
        <div class="text-danger"><?= \app\core\Request::error('password') ?></div>
    </div>
    <button class="btn btn-success">Log In</button>
</form>
</div>