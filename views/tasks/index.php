<?php
    use app\core\route\Url;
    $orderColumn = \app\core\Request::getQueryVal('orderColumn');
?>
<h1>Task List</h1>
<div class="table-responsive">
    <?php if ($_meta['totalCount'] > 0): ?>
        <table class="table stripped">
        <thead>
        <tr>
            <th>NN</th>
            <th>
                <a href="<?= Url::route('tasks.index', Url::getOrderParamsForField('username')) ?>">
                    Username
                    <?php if ($orderColumn == 'username'): ?>
                        <?php if (Url::getOrderDirectionForField('username') == 'asc'): ?>
                            <span class="glyphicon glyphicon-arrow-up"></span>
                        <?php else: ?>
                            <span class="glyphicon glyphicon-arrow-down"></span>
                        <?php endif ?>
                    <?php endif ?>
                </a>
            </th>
            <th>
                <a href="<?= Url::route('tasks.index', Url::getOrderParamsForField('email')) ?>">
                    Email
                    <?php if ($orderColumn == 'email'): ?>
                        <?php if (Url::getOrderDirectionForField('email') == 'asc'): ?>
                            <span class="glyphicon glyphicon-arrow-up"></span>
                        <?php else: ?>
                            <span class="glyphicon glyphicon-arrow-down"></span>
                        <?php endif ?>
                    <?php endif ?>
                </a>
            </th>
            <th>Text</th>
            <th>
                <a href="<?= Url::route('tasks.index', Url::getOrderParamsForField('status'))?>">
                    Status
                    <?php if ($orderColumn == 'status'): ?>
                        <?php if (Url::getOrderDirectionForField('status') == 'asc'): ?>
                            <span class="glyphicon glyphicon-arrow-up"></span>
                        <?php else: ?>
                            <span class="glyphicon glyphicon-arrow-down"></span>
                        <?php endif ?>
                    <?php endif ?>
                </a>
            </th>
            <th>Redacted</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($tasks as $task): ?>
            <tr>
                <td><?= $task->id ?></td>
                <td><?= $task->username ?></td>
                <td><?= $task->email ?></td>
                <td><?= strlen($task->text) > 30 ? substr($task->text, 0, 30) . '...' : $task->text; ?></td>
                <td>
                    <?php if ($task->status): ?>
                        <span class="glyphicon glyphicon-ok-circle text-success"></span>
                    <?php endif ?>
                </td>
                <td>
                    <?php if ($task->edited): ?>
                        <span class="glyphicon glyphicon-ok-circle text-success"></span>
                    <?php endif ?>
                </td>
                <td><a href="<?= \app\core\route\Url::route('tasks.show', ['task' => $task->id]) ?>">View</td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>
    <?php else: ?>
        <div>No data...</div>
    <?php endif ?>
</div>
<?php if ($_meta['totalPages'] > 1): ?>
    <div class="pagination-box">
        <div>Page <?= $_meta['page'] ?> of <?= $_meta['totalPages'] ?>. Total <?= $_meta['totalCount'] ?> items</div>
        <div>
            <a class="btn btn-default" href="<?= $_links['first'] ?>">First</a>
            <a class="btn btn-default" href="<?= $_links['prev'] ?>">Prev</a>
            <a class="btn btn-default" href="<?= $_links['next'] ?>">Next</a>
            <a class="btn btn-default" href="<?= $_links['last'] ?>">Last</a>
        </div>
    </div>
<?php endif ?>
