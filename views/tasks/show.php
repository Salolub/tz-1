<?php use app\core\route\Url; ?>
<h1>View Task N <?= $task->id ?></h1>
<div class="col-sm-offset-3 col-sm-6">
    <div>
        <label>Username</label>
        <div><?= $task->username ?></div>
    </div>
    <div>
        <label>Email</label>
        <div><?= $task->email ?></div>
    </div>
    <div>
        <label>Text</label>
        <div><?= $task->text ?></div>
    </div>
    <div class="mt-1">
        <?php if ($task->status): ?>
            <span class="text-success">Task completed.</span>
        <?php else: ?>
            <span class="text-default">Task not completed yet.</span>
        <?php endif ?>
        &nbsp;&nbsp;&nbsp;
        <?php if ($task->edited): ?>
            <span class="text-success">Task redacted by admin.</span>
        <?php else: ?>
            <span class="text-default">Task not redacted by admin yet.</span>
        <?php endif ?>
    </div>
    <div class="mt-1">
        <?php if (\app\core\Auth::isAdmin()): ?>
            <a class="btn btn-primary"
               href="<?= Url::route('tasks.edit', ['task' => $task->id]) ?>">
                Edit
            </a>
        <?php endif ?>
        <a href="<?= Url::route('tasks.index') ?>" class="btn btn-default">Tasks</a>
    </div>
</div>
