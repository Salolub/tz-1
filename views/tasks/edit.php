<?php
    use app\core\Request;
    use app\core\route\Url;
?>

<h1>Edit Task N <?= $task->id ?></h1>
<form action="<?= Url::route('tasks.update', ['task' => $task->id]) ?>" method="post">
    <input type="hidden" name="_csrf" value="<?= Request::csrf() ?>">
    <input type="hidden" name="_method" value="put">
    <div>
        <label for="username">Username</label>
        <input type="text" id="username" name="username" class="form-control"
               value="<?= Request::getPostVal('username')?>">
        <div class="text-danger"><?= Request::error('username') ?></div>
    </div>
    <div>
        <label for="email">Email</label>
        <input type="text" id="email" name="email" class="form-control"
               value="<?= Request::getPostVal('email')?>">
        <div class="text-danger"><?= Request::error('email') ?></div>
    </div>
    <div>
        <label for="text">Text</label>
        <textarea id="text" name="text" class="form-control" rows="5"
        ><?= Request::getPostVal('text')?></textarea>
        <div class="text-danger"><?= Request::error('text') ?></div>
    </div>
    <div class="mt-1">
        <?php if ($task->status): ?>
            <span class="text-success">Task completed.</span>
        <?php else: ?>
            <span class="text-default">Task not completed yet.</span>
        <?php endif ?>
        &nbsp;&nbsp;&nbsp;
        <?php if ($task->edited): ?>
            <span class="text-success">Task redacted by admin.</span>
        <?php else: ?>
            <span class="text-default">Task not redacted by admin yet.</span>
        <?php endif ?>
    </div>
    <div>
        <button class="btn btn-success">Save</button>
        <a class="btn btn-default" href="<?= Url::route('tasks.index') ?>">Cancel</a>
        <?php if (!$task->status): ?>
            <button name="btnComplete" class="btn btn-primary">Complete</button>
        <?php endif ?>
    </div>
</form>