<?php use app\core\Request; ?>

<h1>Create task</h1>
<div class="col-sm-offset-3 col-sm-6">
    <form action="<?= \app\core\route\Url::route('tasks.store') ?>" method="POST">
        <!--    <input type="hidden" name="_method" value="put">-->
        <input type="hidden" name="_csrf" value="<?= Request::csrf() ?>">
        <div>
            <label for="username">Username</label>
            <input type="text" id="username" name="username" class="form-control"
                   value="<?= Request::getPostVal('username')?>">
            <div class="text-danger"><?= Request::error('username') ?></div>
        </div>
        <div>
            <label for="email">Email</label>
            <input type="text" id="email" name="email" class="form-control"
                   value="<?= Request::getPostVal('email')?>">
            <div class="text-danger"><?= Request::error('email') ?></div>
        </div>
        <div>
            <label for="text">Text</label>
            <textarea id="text" name="text" class="form-control" rows="5"
            ><?= Request::getPostVal('text')?></textarea>
            <div class="text-danger"><?= Request::error('text') ?></div>
        </div>
        <button class="btn btn-success">Create</button>
    </form>
</div>