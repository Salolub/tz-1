<?php
    use \app\core\route\Url;
    $route = \app\core\Request::getRoute()['name'];
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
    <header>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?= Url::route('home') ?>">Test Framework</a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="<?= $route == 'tasks.index' ? 'active' : '' ?>">
                        <a href="<?= Url::route('tasks.index') ?>">Tasks</a>
                    </li>
                    <li class="<?= $route == 'tasks.create' ? 'active' : '' ?>">
                        <a href="<?= Url::route('tasks.create') ?>">Create Task</a>
                    </li>
                    <li class="<?= $route == 'login.show' ? 'active' : '' ?>">
                        <?php if (\app\core\Auth::isAdmin()): ?>
                            <form action="<?= Url::route('logout') ?>" method="POST">
                                <input type="hidden" name="_csrf" value="<?= \app\core\Request::csrf() ?>">
                                <button>Log Out</button>
                            </form>
                        <?php else: ?>
                            <a href="<?= Url::route('login.show') ?>">Login</a>
                        <?php endif ?>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <div class="container">
        <?php if(isset($flashMessage)): ?>
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?= $flashMessage ?>
            </div>
        <?php endif ?>
        <?php require $subView ?>
    </div>
    <footer>
        <span>&copy; Victor Tkachenko, 2020</span>
    </footer>
</body>