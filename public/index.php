<?php

spl_autoload_register(function ($class) {
    $file = str_replace('\\', DIRECTORY_SEPARATOR, $class).'.php';
    $file = '../' . $file;
    if (file_exists($file)) {
        require $file;
        return true;
    }
    return false;
});

if (!file_exists('../config/local.php')) {
    die('Config file local.php does not exists. 
    See example.local.php for example or copy it into local.php and configure DB settings');
}
$config = array_merge(
    require '../config/main.php',
    require '../config/local.php'
);

try {
    $app = \app\core\App::init($config);
    require_once '../app/common/functions.php';
    $app->run();
} catch (Throwable $e) {
    echo $e->getMessage();
}
